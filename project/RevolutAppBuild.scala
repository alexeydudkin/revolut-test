import sbt.Build
import sbt.Keys._
import sbt._

object RevolutAppBuild extends Build {
  val buildSettings = Seq(
    name := """MoneyTransfer""",
    version := "1.0",
    scalaVersion := "2.11.6",
    libraryDependencies ++= appDependencies,
    resolvers ++= appResolvers
  )

  val appResolvers = Seq(
    Resolver.sonatypeRepo("releases")
  )

  val appDependencies = Seq(
    "com.typesafe.akka" %% "akka-actor" % "2.3.11",
    "com.typesafe.akka" %% "akka-testkit" % "2.3.11" % "test",
    "org.scalatest" %% "scalatest" % "2.2.4" % "test",
    "com.typesafe.akka" % "akka-http-experimental_2.11" % "2.4.8",
    "com.typesafe.akka" % "akka-persistence_2.11" % "2.4.8",
    "com.typesafe.akka" % "akka-http-spray-json-experimental_2.11" % "2.4.8",
    "org.dmonix.akka" % "akka-persistence-mock_2.11" % "1.1.1",
    "com.typesafe.akka" % "akka-slf4j_2.11" % "2.4.8",
    "ch.qos.logback" % "logback-classic" % "1.1.7"
  )

  import com.github.retronym.SbtOneJar._

  lazy val app = (project in file(".")).settings(buildSettings ++ oneJarSettings ++ Seq(
    mainClass in oneJar := Some("com.revolut.AppMain"),
    artifactName in oneJar := {(sv: ScalaVersion, module: ModuleID, artifact: Artifact) ⇒
      "moneytransfer.jar"
    }) :_*)

}
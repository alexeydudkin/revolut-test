package com.revolut.routes

import akka.actor.{Props, ActorSystem}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.util.Timeout
import com.revolut.bl.{TransferManager, AccountManager}
import com.revolut.model._
import akka.pattern.ask
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

trait Routes {
  implicit val actorSystem: ActorSystem
  implicit val executionContext: ExecutionContext

  implicit val timeout = Timeout(5 seconds)

  lazy val accountManager = actorSystem.actorOf(Props[AccountManager], "account-manager")

  lazy val transferManager = actorSystem.actorOf(Props(classOf[TransferManager], accountManager), "transfer-manager")


  import JsonSupport._

  implicit val createAccountJsonFormat = jsonFormat2(CreateAccount.apply)
  implicit val transferFundsJsonFormat = jsonFormat3(TransferFunds.apply)

  def routing =
    path("account"){
      post {
        entity(as[CreateAccount]){ command =>
          onComplete(accountManager ? command) {
            case Success(result) =>
              result match {
                case AccountCreated(account) => complete(account)
              }
            case Failure(error) => complete(InternalServerError)
          }
        }
      }
    } ~
    get {
      path("account" / Segment) { accountId =>
        onComplete(accountManager ? GetAccount(accountId)) {
          case Success(result) =>
            result match {
              case AccountNotFound(_) => complete(NotFound)
              case AccountResponse(account) => complete(account)
            }
          case Failure(error) => complete(InternalServerError)
        }
      }
    } ~
    path("transfer"){
      post {
        entity(as[TransferFunds]){ command =>
          onComplete(transferManager ? command) {
            case Success(result) =>
              result match {
                case TransferResponse(transfer) => complete(transfer)
              }
            case Failure(error) => complete(InternalServerError)
          }
        }
      }
    }

}

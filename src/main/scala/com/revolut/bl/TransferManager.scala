package com.revolut.bl

import akka.actor.{ActorRef, ActorLogging}
import akka.persistence.PersistentActor
import com.revolut.model._

class TransferManager(accountManager: ActorRef) extends PersistentActor with ActorLogging {
  var transfers = Map.empty[String, (Transfer, Option[ActorRef])]

  private def updateState(event: Event, sender: Option[ActorRef]) =
    event match {
      case TransferRecordCreated(transfer) =>
        transfers += transfer.id -> (transfer -> sender)
    }

  override def persistenceId: String = "transfer-manager"

  override def receiveRecover: Receive = {
    case event: Event =>
      updateState(event, None)
  }

  override def receiveCommand: Receive = {
    case TransferFunds(from, to, money) =>
      persist(TransferRecordCreated(Transfer(fromAccountId = from, toAccountId = to, value = money, status = Initiated))){ event =>
        log.debug("Init transfer between {} and {} for {}", from, to, money)
        updateState(event, Some(sender()))
        accountManager ! AccountWithdrawFunds(event.transfer.id, from, money)
      }

    case AccountWithdrawals(transferId, accountId, money) =>
      transfers.get(transferId) foreach {case (transfer, sender) =>
        persist(TransferRecordCreated(transfer.copy(status = WithdrawFunds))){ event =>
          log.debug("Withdrawals {} funds from {}", transfer.value, transfer.fromAccountId)
          updateState(event, sender)
          accountManager ! AccountAddFunds(transferId, transfer.toAccountId, transfer.value)
        }
      }

    case AccountFundsAdded(transferId, accountId, money) =>
      transfers.get(transferId) foreach {case (transfer, sender) =>
        persist(TransferRecordCreated(transfer.copy(status = Closed))){ event =>
          log.debug("Add {} funds to {}", transfer.toAccountId, transfer.value)
          updateState(event, sender)
          sender foreach( _ ! TransferResponse(event.transfer))
        }
      }

    case NotEnoughMoneyResponse(transferId) =>
      transfers.get(transferId) foreach {case (transfer, sender) =>
        persist(TransferRecordCreated(transfer.copy(status = Failed))){ event =>
          updateState(event, sender)
          sender foreach(_ ! TransferResponse(event.transfer))
        }
      }

    case AccountNotFoundResponse(transferId, accountId) =>
      transfers.get(transferId) foreach {case (transfer, sender) =>
        persist(TransferRecordCreated(transfer.copy(status = Failed))){ event =>
          updateState(event, sender)
          if (transfer.status == WithdrawFunds)
            accountManager ! AccountAddFunds(transfer.id, transfer.fromAccountId, transfer.value)
          sender foreach(_ ! TransferResponse(event.transfer))
        }
      }

  }

}

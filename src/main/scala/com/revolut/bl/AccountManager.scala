package com.revolut.bl

import akka.actor.ActorLogging
import akka.persistence.PersistentActor
import com.revolut.model._

class AccountManager extends PersistentActor with ActorLogging {

  var accounts = Set.empty[String]

  private def getAccountActor(accountId: String) =
    context.child(accountId) match {
      case Some(account) =>
        log.debug("Found account {}", accountId)
        account
      case None =>
        log.debug("Creating account {}", accountId)
        context.actorOf(AccountActor.props(Account(accountId, None, 0)), accountId)
    }

  private def updateState(event: Event) = event match {
    case AccountCreated(account) =>
      accounts += account.id
  }

  override def persistenceId: String = "account-manager"

  override def receiveRecover: Receive = {
    case event: Event => updateState(event)
  }

  override def receiveCommand: Receive = {
    case CreateAccount(accountName, initialBalance) =>
      persist(AccountCreated(Account(name = Some(accountName), balance = initialBalance))) { event =>
        log.debug("Account {} created !", event.account)
        updateState(event)
        context.actorOf(AccountActor.props(event.account), event.account.id)
        sender() ! event
      }

    case cmd@GetAccount(accountId) if accounts contains accountId =>
      getAccountActor(accountId) forward cmd

    case GetAccount(accountId) =>
      log.debug("Account {} not found", accountId)
      sender ! AccountNotFound(accountId)

    case cmd@AccountAddFunds(transferId, accountId, money) if accounts contains accountId =>
      getAccountActor(accountId) forward cmd

    case AccountAddFunds(transferId, accountId, _) =>
      log.debug("Account {} not found for add funds operation", accountId)
      sender ! AccountNotFoundResponse(transferId, accountId)

    case cmd@AccountWithdrawFunds(transferId, accountId, money) if accounts contains accountId =>
      getAccountActor(accountId) forward cmd

    case cmd@AccountWithdrawFunds(transferId, accountId, _) =>
      log.debug("Account {} not found for withdraw funds operation", accountId)
      sender ! AccountNotFoundResponse(transferId, accountId)
  }

}

package com.revolut.bl

import akka.actor.{Props, ActorLogging}
import akka.persistence.PersistentActor
import com.revolut.model._
import com.revolut.model.Account

class AccountActor(var account: Account) extends PersistentActor with ActorLogging {

  private def updateState(event: Event) = event match {
    case AccountFundsAdded(_, _, money)  =>
      account = account.copy(balance = account.balance + money)

    case AccountWithdrawals(_, _, money) =>
      account = account.copy(balance = account.balance - money)

  }

  override def persistenceId: String = s"account-${account.id}"

  override def receiveRecover: Receive = {
    case event: Event => updateState(event)
  }

  override def receiveCommand: Receive = {
    case GetAccount(_) =>
      sender ! AccountResponse(account)

    case AccountAddFunds(transferId, accountId, money) =>
      persist(AccountFundsAdded(transferId, accountId, money)){ event =>
        log.debug("Add {} for account {}", money, accountId)
        updateState(event)
        sender ! event

      }

    case AccountWithdrawFunds(transferId, accountId, money) =>
      if(account.balance >= money)
        persist(AccountWithdrawals(transferId, accountId, money)){event =>
          updateState(event)
          sender ! event
        } else {
          sender ! NotEnoughMoneyResponse(transferId)
      }
  }

}

object AccountActor {
  def props(account: Account) = Props(classOf[AccountActor], account)
}

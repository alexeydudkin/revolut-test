package com.revolut

import akka.actor.{Props, ActorSystem}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.revolut.bl.{TransferManager, AccountManager}
import com.revolut.routes.Routes
import scala.io.StdIn

object AppMain extends App with Routes with Cfg {
  implicit val actorSystem = ActorSystem("MoneyTransfer")
  implicit val executionContext = actorSystem.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val serverBinding = Http().bindAndHandle(
    handler = routing,
    interface = httpInterface,
    port = httpPort
  )

  println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine()
  serverBinding
    .flatMap(_.unbind())
    .onComplete(_ => actorSystem.terminate())
}

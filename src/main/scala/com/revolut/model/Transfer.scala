package com.revolut.model

import java.util.UUID

case class Transfer(
                    id: String = UUID.randomUUID().toString,
                    fromAccountId: String,
                    toAccountId: String,
                    value: BigDecimal,
                    status: TransferStatus
                   )

sealed trait TransferStatus {
  def message: String
}
object TransferStatus {
  def apply(message: String) = message match {
    case "Transfer inited" => Initiated
    case "Transfer failed" => Failed
    case "Transfer closed" => Closed
    case "Funds Withdrawals" => WithdrawFunds
  }
}
case object Initiated extends TransferStatus {
  override def message: String = "Transfer inited"
}
case object Failed extends TransferStatus {
  override def message: String = "Transfer failed"
}
case object Closed extends TransferStatus {
  override def message: String = "Transfer closed"
}
case object WithdrawFunds extends TransferStatus {
  override def message: String = "Funds Withdrawals"
}

package com.revolut

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{JsString, JsValue, RootJsonFormat, DefaultJsonProtocol}

package object model {

  object JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
    implicit val transferJsonFormat = jsonFormat5(Transfer.apply)
    implicit val accountJsonFormat = jsonFormat3(Account.apply)

    implicit object TransferStatusFormat extends RootJsonFormat[TransferStatus]{
      override def write(status: TransferStatus): JsValue = JsString(status.message)

      override def read(json: JsValue): TransferStatus = TransferStatus(json.toString{case JsString(value) => value})
    }

  }

  trait Event
  trait Command

  case class CreateAccount(accountName: String, balance: BigDecimal) extends Command
  case class AccountCreated(account: Account) extends Event

  case class AccountAddFunds(transferId: String, accountId: String, money: BigDecimal) extends Command
  case class AccountFundsAdded(transferId: String, accountId: String, money: BigDecimal) extends Event

  case class AccountWithdrawFunds(transferId: String, accountId: String, money: BigDecimal) extends Command
  case class AccountWithdrawals(transferId: String, accountId: String, money: BigDecimal) extends Event

  case class TransferFunds(fromAccountId: String, toAccountId: String, money: BigDecimal) extends Command
  case class TransferRecordCreated(transfer: Transfer) extends Event

  case class GetAccount(accountId: String) extends Command

  case class AccountNotFound(accountId: String)

  case class AccountNotFoundResponse(transferId: String, accountId: String)

  case class NotEnoughMoneyResponse(transferId: String)

  case class AccountResponse(account: Account)

  case class TransferResponse(transfer: Transfer)

}

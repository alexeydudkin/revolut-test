package com.revolut.model

import java.util.UUID

case class Account(
                    id: String = UUID.randomUUID().toString,
                    name: Option[String] = None,
                    balance: BigDecimal
                  )


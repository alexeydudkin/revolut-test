package com.revolut

import akka.actor.{Props, ActorSystem}
import akka.testkit.{ImplicitSender, TestKit}
import com.revolut.bl.AccountManager
import com.revolut.model._
import org.scalatest.{WordSpecLike, BeforeAndAfterAll, Matchers}

class AccountManagerSpec extends TestKit(ActorSystem("Account-Manager-Spec")) with WordSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender {
  val worker = system.actorOf(Props[AccountManager], "account-manager")


  "Account manager" must {
    "create account" in {
      worker ! CreateAccount("user 1", 100)
      expectMsgPF() {
        case AccountCreated(Account(id, Some("user 1"), balance)) if balance == BigDecimal(100) => true
      }
    }

    "add funds" in {
      worker ! CreateAccount("user 2", 100)
      expectMsgPF() {
        case AccountCreated(Account(id, name, balance)) =>
          worker ! AccountAddFunds("transfer-1", id, 100)
          expectMsgPF() {
            case AccountFundsAdded(_, accountId, _) =>
              worker ! GetAccount(accountId)
              expectMsgPF() {
                case AccountResponse(account) if account.balance == BigDecimal(200) => true
              }
          }
      }
    }

    "withdraw funds" in {
      worker ! CreateAccount("user 3", 100)
      expectMsgPF() {
        case AccountCreated(Account(id, name, balance)) =>
          worker ! AccountWithdrawFunds("transfer-2", id, 100)
          expectMsgPF() {
            case AccountWithdrawals(_, accountId, _) =>
              worker ! GetAccount(accountId)
              expectMsgPF() {
                case AccountResponse(account) if account.balance == BigDecimal(0) => true
              }
          }
      }
    }

    "return not enough money" in {
      worker ! CreateAccount("user 4", 100)
      expectMsgPF() {
        case AccountCreated(Account(id, name, balance)) =>
          worker ! AccountWithdrawFunds("transfer-3", id, 150)
          expectMsgPF() {
            case NotEnoughMoneyResponse("transfer-3") => true
          }
      }
    }
  }

  override protected def afterAll() {
    TestKit.shutdownActorSystem(system)
  }
}

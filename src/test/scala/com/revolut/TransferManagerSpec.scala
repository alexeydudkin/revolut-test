package com.revolut

import akka.actor.{Props, ActorSystem}
import akka.testkit.{TestProbe, ImplicitSender, TestKit}
import com.revolut.bl.TransferManager
import com.revolut.model._
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class TransferManagerSpec extends TestKit(ActorSystem("Transfer-Manager-Spec")) with WordSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender {
  val accountManager = TestProbe()

  val transferManager = system.actorOf(Props(classOf[TransferManager], accountManager.ref), "transfer-manager")

  "Transfer manager" must {
    "successfully complete transfer" in {
      transferManager ! TransferFunds("user1", "user2", 100)
      accountManager.expectMsgPF() {
        case AccountWithdrawFunds(transferId, accountId, money) =>
          accountManager.reply(AccountWithdrawals(transferId, accountId, money))

          accountManager.expectMsgPF() {
            case AccountAddFunds(transfer, accountId, money) =>
              accountManager.reply(AccountFundsAdded(transferId, accountId, money))

              expectMsgPF() {
                case TransferResponse(Transfer(_, "user1", "user2", _, Closed)) => true
              }
          }
      }
    }

    "failed due to not enough money" in {
      transferManager ! TransferFunds("user1", "user2", 100)
      accountManager.expectMsgPF() {
        case AccountWithdrawFunds(transferId, accountId, money) =>
          accountManager.reply(NotEnoughMoneyResponse(transferId))

          expectMsgPF() {
            case TransferResponse(Transfer(_, "user1", "user2", _, Failed)) => true
          }
      }
    }

    "return money if destination account not found" in {
      transferManager ! TransferFunds("user1", "user2", 100)
      accountManager.expectMsgPF() {
        case AccountWithdrawFunds(transferId, accountId, money) =>
          accountManager.reply(AccountWithdrawals(transferId, accountId, money))

          accountManager.expectMsgPF() {
            case AccountAddFunds(transferId, "user2", money) =>
              accountManager.reply(AccountNotFoundResponse(transferId, accountId))

              accountManager.expectMsgPF() {
                case AccountAddFunds(transferId, "user1", money) =>

                  expectMsgPF() {
                    case TransferResponse(Transfer(_, "user1", "user2", _, Failed)) => true
                  }
              }
          }
      }
    }
  }

  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }
}
